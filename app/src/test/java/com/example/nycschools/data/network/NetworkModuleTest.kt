package com.example.nycschools.data.network

import junit.framework.TestCase

/**
 * @author Zheng Li
 * 2022/11/12
 */
class NetworkModuleTest : TestCase() {

    fun testProvideApiService() {
        assertNotNull(NetworkModule.provideApiService())
    }
}