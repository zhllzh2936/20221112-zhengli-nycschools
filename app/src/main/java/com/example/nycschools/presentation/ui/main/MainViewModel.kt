package com.example.nycschools.presentation.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.data.repository.NetworkRepository
import com.example.nycschools.data.entity.SchoolInfo
import com.example.nycschools.presentation.base.ScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Zheng Li
 * 2022/11/11
 */
@HiltViewModel
class MainViewModel @Inject constructor(private val repository: NetworkRepository) :
    ViewModel() {

    val state = MutableLiveData(ScreenState.LOADING)
    val schoolList = mutableListOf<SchoolInfo>()
    var selection: SchoolInfo? = null

    fun getSchoolInfoList() {
        state.postValue(ScreenState.LOADING)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val res = repository.getSchoolInfoList()
                if (res?.size == 0) {
                    state.postValue(ScreenState.EMPTY)
                } else {
                    schoolList.clear()
                    res?.let { schoolList.addAll(it) }
                    state.postValue(ScreenState.SUCCESS)
                }
            } catch (e: Exception) {
                state.postValue(ScreenState.ERROR)
            }
        }
    }

}