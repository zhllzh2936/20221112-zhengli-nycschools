package com.example.nycschools.presentation.ui.schools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.data.entity.SchoolInfo
import com.example.nycschools.presentation.adapter.SchoolInfoAdapter
import com.example.nycschools.presentation.base.ScreenState
import com.example.nycschools.presentation.ui.detail.DetailFragment
import com.example.nycschools.presentation.ui.main.MainViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsFragment : Fragment(), SchoolInfoAdapter.ItemListener {

    companion object {
        fun newInstance() = SchoolsFragment()
    }

    private val parentViewModel: MainViewModel by activityViewModels()
    private var list: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var errorMsg: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.schools_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initObserver()
        parentViewModel.getSchoolInfoList()
    }

    private fun initView(view: View) {
        list = view.findViewById(R.id.ls_schools)
        progressBar = view.findViewById(R.id.progress_bar)
        errorMsg = view.findViewById(R.id.error_msg)
        view.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            parentViewModel.getSchoolInfoList()
        }
        list?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        list?.adapter = SchoolInfoAdapter(mutableListOf(), this)
    }

    private fun initObserver() {
        parentViewModel.state.observe(viewLifecycleOwner) {
            when (it) {
                ScreenState.LOADING -> {
                    progressBar?.visibility = View.VISIBLE
                    list?.visibility = View.GONE
                    errorMsg?.visibility = View.GONE
                }
                ScreenState.SUCCESS -> {
                    (list?.adapter as SchoolInfoAdapter).setItem(parentViewModel.schoolList)
                    progressBar?.visibility = View.GONE
                    list?.visibility = View.VISIBLE
                    errorMsg?.visibility = View.GONE
                }
                ScreenState.ERROR, ScreenState.EMPTY -> {
                    progressBar?.visibility = View.GONE
                    list?.visibility = View.GONE
                    errorMsg?.visibility = View.VISIBLE
                    errorMsg?.text = it.toString()
                }
                else -> {}
            }
        }
    }

    override fun onItemClicked(school: SchoolInfo?) {
        parentViewModel.selection = school
        parentFragmentManager.beginTransaction().add(
            R.id.container, DetailFragment.newInstance(),
        ).addToBackStack(null).commit()
    }

}