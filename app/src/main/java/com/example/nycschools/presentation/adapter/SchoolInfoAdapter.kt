package com.example.nycschools.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.data.entity.SchoolInfo

/**
 * @author Zheng Li
 * 2022/11/11
 */
class SchoolInfoAdapter(
    private val schoolInfoList: MutableList<SchoolInfo>,
    private val listener: ItemListener
) : RecyclerView.Adapter<SchoolInfoAdapter.SchoolInfoViewHolder>() {

    class SchoolInfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val schoolName = itemView.findViewById<TextView>(R.id.school_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolInfoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_school, parent, false)
        return SchoolInfoViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolInfoViewHolder, position: Int) {
        holder.schoolName.text = schoolInfoList[position].school_name
        holder.itemView.setOnClickListener {
            listener.onItemClicked(schoolInfoList.getOrNull(position))
        }
    }

    override fun getItemCount(): Int {
        return schoolInfoList.size
    }

    fun setItem(list: List<SchoolInfo>) {
        schoolInfoList.clear()
        schoolInfoList.addAll(list)
        notifyDataSetChanged()
    }

    interface ItemListener {
        fun onItemClicked(school: SchoolInfo?)
    }
}