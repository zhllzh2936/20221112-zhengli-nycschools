package com.example.nycschools.presentation.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.data.repository.NetworkRepository
import com.example.nycschools.data.entity.SATResult
import com.example.nycschools.presentation.base.ScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Zheng Li
 * 2022/11/11
 */
@HiltViewModel
class DetailViewModel @Inject constructor(private val repository: NetworkRepository) : ViewModel() {

    val state = MutableLiveData(ScreenState.LOADING)
    var satResult: SATResult? = null

    fun getSATByDBN(dbn: String?) {
        state.postValue(ScreenState.LOADING)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                satResult = repository.getSATByDBN(dbn)
                if (satResult != null) {
                    state.postValue(ScreenState.SUCCESS)
                } else {
                    state.postValue(ScreenState.EMPTY)
                }
            } catch (e: Exception) {
                state.postValue(ScreenState.ERROR)
            }

        }
    }

}