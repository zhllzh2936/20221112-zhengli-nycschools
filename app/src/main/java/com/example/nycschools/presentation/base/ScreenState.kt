package com.example.nycschools.presentation.base

/**
 * @author Zheng Li
 * 2022/11/11
 */
enum class ScreenState {
    LOADING,
    SUCCESS,
    ERROR,
    EMPTY
}