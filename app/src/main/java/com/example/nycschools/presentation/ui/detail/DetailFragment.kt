package com.example.nycschools.presentation.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.nycschools.R
import com.example.nycschools.presentation.base.ScreenState
import com.example.nycschools.presentation.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {

    companion object {
        fun newInstance() = DetailFragment()
    }

    private var writing: TextView? = null
    private var reading: TextView? = null
    private var math: TextView? = null
    private var name: TextView? = null
    private var progressBar: ProgressBar? = null
    private var errorMsg: TextView? = null
    private var detail: LinearLayout? = null
    private val viewModel: DetailViewModel by lazy {
        ViewModelProvider(this)[DetailViewModel::class.java]
    }
    private val parentViewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initObserver()
        viewModel.getSATByDBN(parentViewModel.selection?.dbn)
    }

    private fun initObserver() {
        viewModel.state.observe(viewLifecycleOwner) {
            when (it) {
                ScreenState.SUCCESS -> {
                    detail?.visibility = View.VISIBLE
                    progressBar?.visibility = View.GONE
                    errorMsg?.visibility = View.GONE
                    math?.text = viewModel.satResult?.sat_math_avg_score
                    reading?.text = viewModel.satResult?.sat_critical_reading_avg_score
                    writing?.text = viewModel.satResult?.sat_writing_avg_score
                    name?.text = viewModel.satResult?.school_name
                }
                ScreenState.LOADING -> {
                    detail?.visibility = View.GONE
                    progressBar?.visibility = View.VISIBLE
                    errorMsg?.visibility = View.GONE
                }
                ScreenState.ERROR, ScreenState.EMPTY -> {
                    detail?.visibility = View.GONE
                    progressBar?.visibility = View.GONE
                    errorMsg?.visibility = View.VISIBLE
                    errorMsg?.text = it.toString()
                }
                else -> {}
            }

        }
    }

    private fun initView(view: View) {
        math = view.findViewById(R.id.math)
        reading = view.findViewById(R.id.reading)
        writing = view.findViewById(R.id.writing)
        name = view.findViewById(R.id.name)
        progressBar = view.findViewById(R.id.progress_bar)
        errorMsg = view.findViewById(R.id.error_msg)
        detail = view.findViewById(R.id.detail)
    }
}