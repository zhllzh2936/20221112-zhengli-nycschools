package com.example.nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author Zheng Li
 * 2022/11/12
 */
@HiltAndroidApp
class NYCSchoolsApp : Application()