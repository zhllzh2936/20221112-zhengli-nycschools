package com.example.nycschools.data.network

import com.example.nycschools.data.entity.SATResultList
import com.example.nycschools.data.entity.SchoolInfoList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Zheng Li
 * 2022/11/11
 */
interface ApiService {

    /**
     * get list of schools
     * @return list of SchoolInfo
     */
    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchoolInfoList(): Response<SchoolInfoList>

    /**
     * get SAT scores of the school
     * @param dbn the dbn of the school
     * @return the school's SAT result
     */
    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATByDBN(@Query("dbn")dbn: String?): Response<SATResultList>

}