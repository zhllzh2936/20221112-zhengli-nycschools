package com.example.nycschools.data.entity

/**
 * @author Zheng Li
 * 2022/11/11
 */
class SchoolInfoList : ArrayList<SchoolInfo>()