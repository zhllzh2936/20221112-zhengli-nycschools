package com.example.nycschools.data.repository

import android.util.Log
import com.example.nycschools.data.network.ApiService
import com.example.nycschools.data.entity.SATResult
import com.example.nycschools.data.entity.SchoolInfoList
import javax.inject.Inject

/**
 * @author Zheng Li
 * 2022/11/11
 */
class NetworkRepository @Inject constructor(private val service: ApiService) {

    companion object {
        private val TAG: String = this::class.java.simpleName
    }

    suspend fun getSchoolInfoList(): SchoolInfoList? {
        val res = service.getSchoolInfoList().body()
        Log.d(TAG, res.toString())
        return res
    }

    suspend fun getSATByDBN(dbn: String?): SATResult? {
        val res = service.getSATByDBN(dbn).body()
        Log.d(TAG, res.toString())
        return res?.getOrNull(0)
    }
}